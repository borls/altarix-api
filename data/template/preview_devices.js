﻿'use strict';

/*
    Список девайсов для предпросмотра
    Разрешение желательно указывать реальное и затем scal'ить до необходимого размера
    абсолютная позиция задается от указанного изображения
*/

export default {
    list: [
        {name: "browser", label: "Компьютер",
            styles: {
                width: "708px",
                height: "474px",
                position: "absolute",
                left: "0px",
                top: "23px"
            },
            img: '/api/v1/template/email/device/img/browser'
        },
        {name: "phone", label: "Телефон",
            styles: {
                width: "640px",
                height: "1092px",
                position: "absolute",
                left: "23px",
                top: "94px",
                transform: "scale(0.3401)",
                transformOrigin: "left top 0px"
            },
            img: '/api/v1/template/email/device/img/iphone'
        },
        {name: "tablet", label: "Планшет",
            styles: {
                width: "1024px",
                height: "768px",
                position: "absolute",
                left: "62px",
                top: "30px",
                transform: "scale(0.5655, 0.5665)",
                transformOrigin: "left top 0px"
            },
            img: '/api/v1/template/email/device/img/ipad'
        }
    ],
    default: "browser"
};