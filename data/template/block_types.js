﻿'use strict';

/*
    Типы параметров:

    'number' со стандартным значением 0
        или
    {
        type: 'number',
        min: 0,
        max: 100,
        step: 0.2,
        default: 0
    },

    'text'
        или
    {
        type: 'text',
        default: ''
    },

    {
        type: 'dropdown',
        values: ['a', 'b', 'c'],
        default: 'a'
    },

    'url' // испольузется валидация URL
        или
    {
        type: 'url',
        default: ''
    }
*/

export default [
    {
        name: 'Header',
        editorHeight: '100px',
        html: '<{params.tag} style="font-size: {params.font_size}; color: {params.color};">{params.text}</{params.tag}>',
        preview: '<{params.tag}>{params.text}</{params.tag}>',
        img: '/api/v1/template/images/template_header.png',

        /*
            Different param types
            Если тип для параметра не указан, используется text
        */
        params: {
            font_size: {
                type: 'range',
                min: '2',
                max: '100',
                units: 'px',
                default: 16,
                label: 'Размер шрифта'
            },
            text: {
                type: 'text',
                label: 'Текст'
            },
            tag: {
                type: 'dropdown',
                values: ['h1', 'h2', 'h3', 'h4', 'p'],
                default: 'h1',
                label: 'Тег'
            },
            color: {
                type: 'color',
                label: 'Цвет'
            }
        }
    },
    {
        name: 'Splitter',
        editorHeight: '100px',
        html: '<hr>',
        preview: '',
        img: '/api/v1/template/images/template_splitter.png'
    },
    {
        name: 'Greetings',
        editorHeight: '200px',
        html: '<p style="color: {params.color}">Здравствуйте {params.тест} {globals.name}!</p>',
        preview: '<p>Здравствуйте ИМЯ!</p>',
        img: '/api/v1/template/images/template_block.png',
        params: {
            color: {
                type: 'color',
                label: 'Цвет'
            }
        }
    },
    {
        name: 'Image',
        editorHeight: '100px',
        html: '<img src="{params.src}"></img>',
        preview: '<img src="{params.src}"></img>',
        img: '/api/v1/template/images/template_image.png',
        params: {
            src: {
                type: 'url',
                label: 'Ссылка на изображение'
            }
        }
    },
    {
        name: 'Text',
        editorHeight: '100px',
        html: '<p>{params.text}</p>',
        preview: '<p>{params.text}</p>',
        img: '/api/v1/template/images/template_text.png',
        params: {
            text: {
                type: 'text',
                label: 'Текст'
            }
        }
    },
    {
        name: 'Labeled image',
        editorHeight: '100px',
        img: '/api/v1/template/images/template_image_label.png',
        html: `
            <h3>{params.label}</h3>
            <img src="{params.src}"></img>
        `,
        preview: `
            <h3>{params.label}</h3>
            <img src="{params.src}"></img>
        `,
        params: {
            label: {
                type: 'text',
                label: 'Надпись'
            },
            src: 'url'
        }
    },
    {
        name: 'CSS',
        editorHeight: '100px',
        html: `<style>{params.css}</style>`,
        img: '/api/v1/template/images/template_css.png',
        preview: `<p>style: <pre>{params.css}</pre></p>`,
        params: {
            css: 'text'
        }
    },
    {
        name: 'Input form',
        label: 'Форма ввода',
        editorHeight: '100px',
        img: '/api/v1/template/images/template_contact.png',
        html: `
            <h3>{params.header}</h3>
            <input placeholder="{params.name_placeholder}" />
            <input placeholder="{params.surname_placeholder}" />
        `,
        preview: `
            <h3>{params.header}</h3>
            <input readonly placeholder="{params.name_placeholder}" />
            <input readonly placeholder="{params.surname_placeholder}" />
        `,
        params: {
            header: 'text'
        }
    }
];
