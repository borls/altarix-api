'use strict';

import express from 'express';
const router = express.Router();

import Http from '../http';
import Query from '../query';

import am2 from '../../data/auditory_metrics/2';

import {
    registerAuth,
    registerCampaigns,
    registerCommunicationChannels,
    registerGroups,
    registerStats,
    registerTemplates,
    registerUsers,
    registerGroups_Subjects
} from './routes/index.js';

registerAuth(router);
registerCampaigns(router);
registerCommunicationChannels(router);
registerGroups(router);
registerStats(router);
registerTemplates(router);
registerUsers(router);
registerGroups_Subjects(router);


router.get('/api/v1/error', (req, res) => {
    const text = req.query.text || 'Специальная ошибка';
    const code = req.query.code || 1337;
    res.api([], code, text);
});

router.get('/api/v1/auditory_metrics/2', (req, res) => res.api(am2));

const component = new Http({table: 'component'});
router.get('/api/v1/component/list', component.getList);
router.get('/api/v1/component', component.getItem);


export default router;
