import http from 'http';
import Service from '../service';
import socketIO from 'socket.io';

export default (app) => {
  const httpServer = http.createServer(app);
  const io = socketIO(httpServer);

  (new Service({table: 'group'})).liveUpdates(io);
  return httpServer;
};