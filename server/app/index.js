import path from 'path';
import express from 'express';
import * as middleware from './middlewares';
import routes from './routes';
import bodyParser from 'body-parser';
import favicon from 'serve-favicon';
import io from './io';

const app = express()
  .use(middleware.api)
  .use(middleware.cors)
  .use(middleware.auth)
  .use(bodyParser.urlencoded({ extended: true }))
  .use(bodyParser.json())
  .use(routes)
  .use(favicon(path.join(__dirname, 'images', 'favicon.ico')));


export default io(app);
