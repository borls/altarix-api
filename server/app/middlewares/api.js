var _isArray = require('lodash.isarray');

export default function (req, res, next) {
  res.api = (result = [], errorCode = 0, errorMessage = '') =>
    res.json({
      errorCode,
      errorMessage,
      result: _isArray(result) && result || [ result ]
    });
  next();
};
