import api from '../middlewares/api.js';
import cors from '../middlewares/cors.js';
import auth from '../middlewares/auth.js';

export { api, cors, auth };