var basicAuth = require('basic-auth');

var auth = function (req, res, next) {

    return next();
    var user = basicAuth(req);

    if (user && user.name === 'admin' && user.pass === '123321') {
        return next();
    } else {
        res.set('WWW-Authenticate', 'Basic realm=Authorization Required');
        return res.api({}, 401, 'Authorization Required');
    }
};

module.exports = auth;