import express from 'express';
var path = require('path');

let router = express.Router();

import Http from '../http';


import communication_channels from '../data/communication_channels';
import campaign from '../data/campaign/list';
import campaignItem from '../data/campaign/571a5cfcb42b94a16d8b4567';
import am2 from '../data/auditory_metrics/2';
import profile from '../data/profile';

import templateList from '../../data/template/list';
const templateItem = (id) => require('../../data/template/' + id);

import userList from '../../data/user/list';
const userItem = (id) => require('../../data/user/' + id);

import eventList from '../data/event/list';
import attributeList from '../data/attribute/list';


import groupList from '../../data/group/list';
const groupItem = (id) => require('../../data/group/' + id);

import groupImportUpload from '../../data/group/import/upload';
import groupImportCreate from '../../data/group/import/create';
import groupExport from '../../data/group/export';
import groupFilter from '../../data/group/filter';
const groupFilterId = (id) => require(`../../data/group/filter/${id}`);






/**
 * Authorization
 */
router.post('/api/v1/user/signin', function (req, res) {
  const token = req.headers.authorization;
  console.log('Token from API', token);
  res.header('Authorization', token);
  res.api({});
});

router.post('/api/v1/checkLogin', function (req, res) {
  res.api({});
});



/**
 * Methods
 */
router.get('/api/v1/communication_channels', function (req, res) {
  res.api(communication_channels);
});
router.get('/api/v1/companies/list', function (req, res) {
  res.api(campaign);
});
router.get('/api/v1/companies', function (req, res) { //?id=571a5cfcb42b94a16d8b4567
  res.api(require('../data/campaign/' + req.query.id));
});
router.get('/api/v1/auditory_metrics/2', function (req, res) {
  res.api(am2);
});
router.get('/api/v1/profile', function (req, res) {
  res.api(profile);
});
router.get('/api/v1/group/list', function (req, res) {
  res.api(groupList);

});
router.get('/api/v1/group', function (req, res) { // ?id=571a5cfcb42b94a16d8b4567
  res.api(require('../data/group/' + req.query.id));
});

/**
 * Templates
 */
router.get('/api/v1/templates/list', function (req, res) {
  res.api(templateList);
});
router.get('/api/v1/templates', function (req, res) { // ?id=571a5cfcb42b94a16d8b4567
  res.api(templateItem(req.query.id));
});

/**
 * Users
 */
router.get('/api/v1/user/list', function (req, res) {
  res.api(userList);
});
router.get('/api/v1/user/get', function (req, res) { // ?id=571a5cfcb42b94a16d8b4567
  res.api(userItem(req.query.id));
});

/**
 * Events
 */
router.get('/api/v1/groups/events/list', function (req, res) {
  res.api(eventList);
});


/**
 * Attributes
 */
router.get('/api/v1/groups/attributes/list', function (req, res) {
  res.api(attributeList);
});

export default router;
