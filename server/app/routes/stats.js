'use strict';

import notpurpose_nondelivery from '../../../data/stats/notpurpose_nondelivery';
import notpurpose_passedlinks from '../../../data/stats/notpurpose_passedlinks';
import stats_map_data from '../../../data/stats/map_random';
import purposeActivity from '../../../data/stats/activity';
import sex_diagram from '../../../data/stats/sex_diagram';

export default function registerStats(router) {
    /*
        Статистика по всем кампаниям
    */

    function stats_notpurpose_nondelivery(req, res) {
        if(req.params.campaignId) console.log('/stats/notpurpose-nondelivery', 'stats for', req.params.campaignId);
        res.api(notpurpose_nondelivery);
    }

    function stats_notpurpose_passedlinks(req, res) {
        if(req.params.campaignId) console.log('/stats/notpurpose-passedlinks', 'stats for', req.params.campaignId);
       res.api(notpurpose_passedlinks);
    }

    function stats_map(req, res) {
        if(req.params.campaignId) console.log('/stats/map', 'stats for', req.params.campaignId);
        let num = 3;
        let result = [];
        for(let i = 0; i < 3; ++i) {
            let index = Math.floor(Math.random() * stats_map_data.length);
            result.push(stats_map_data[index]);
        }
        res.api(result);
    }

    function stats_activity(req, res) {
        if(req.params.campaignId) console.log('/stats/activity', 'stats for', req.params.campaignId);
        res.api(purposeActivity);
    }

    function stats_time(req, res) {
        if(req.params.campaignId) console.log('/stats/time', 'stats for', req.params.campaignId);
        res.api({
          "totalSend": 690922,
          "totalAction": 291828,
          "data": [
            {"name": "5", "count": 2},
            {"name": "10", "count": 0},
            {"name": "30", "count": 1},
            {"name": "60", "count": 1},
            {"name": ">60", "count": 291827}
          ]
        });
    }

    function stats_age_diagram(req, res) {
        if(req.params.campaignId) console.log('/stats/age-diagram', 'stats for', req.params.campaignId);
        res.api([
            { "name": "0-6", "count": 1239, "all": 8275 },
            { "name": "7-14", "count": 1777, "all": 9580 },
            { "name": "15-18", "count": 3698, "all": 20966 },
            { "name": "19-30", "count": 31181, "all": 123358 },
            { "name": "31-49", "count": 60511, "all": 214223 },
            { "name": "50+", "count": 44932, "all": 132862 }
        ]);
    }

    function stats_sex_diagram(req, res) {
        if(req.params.campaignId) console.log('/stats/sex-diagram', 'stats for', req.params.campaignId);
        res.api(sex_diagram);
    }

    function stats_sender(req, res) {
        if(req.params.campaignId) console.log('/stats/sender', 'stats for', req.params.campaignId);
        res.api({});
    }

    router.get('/api/v1/campaign/list/stats/notpurpose-nondelivery', stats_notpurpose_nondelivery);
    router.get('/api/v1/campaign/item/:campaignId/stats/notpurpose-nondelivery', stats_notpurpose_nondelivery);

    router.get('/api/v1/campaign/list/stats/notpurpose-passedlinks', stats_notpurpose_passedlinks);
    router.get('/api/v1/campaign/item/:campaignId/stats/notpurpose-passedlinks', stats_notpurpose_passedlinks);

    router.get('/api/v1/campaign/list/stats/map', stats_map);
    router.get('/api/v1/campaign/item/:campaignId/stats/map', stats_map);

    router.get('/api/v1/campaign/list/stats/activity', stats_activity);
    router.get('/api/v1/campaign/item/:campaignId/stats/activity', stats_activity);

    router.get('/api/v1/campaign/list/stats/time', stats_time);
    router.get('/api/v1/campaign/item/:campaignId/stats/time', stats_time);

    router.get('/api/v1/campaign/list/stats/age-diagram', stats_age_diagram);
    router.get('/api/v1/campaign/item/:campaignId/stats/age-diagram', stats_age_diagram);

    router.get('/api/v1/campaign/list/stats/sex-diagram', stats_sex_diagram);
    router.get('/api/v1/campaign/item/:campaignId/stats/sex-diagram', stats_sex_diagram);

    /*router.get('/api/v1/campaign/list/stats/sender', stats_sender);
    router.get('/api/v1/campaign/item/:campaignId/stats/sender', stats_sender);*/
};
