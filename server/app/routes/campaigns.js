'use strict';

import campaignList from '../../../data/campaign/list'

/*
    Кампании
*/

// Список кампаний

export default function registerCampaigns(router) {
    router.get('/api/v1/campaign/list', function (req, res) {
        res.api(campaignList);
    });

    router.post('/api/v1/campaign/create', function(req, res) {
        const { name, groupName } = req.body.data;

        campaignList.push({
            id: Math.floor(Math.random() * 879456123) + 'wzrd',
            name,
            groupName,
            description: 'Подключено к группе ' + groupName,
            status: 'STOPPED',
            beginDate: new Date().getTime() / 1000,
            endDate: null,
            auditory: 0,
            potentialAuditory: 0,
            locked: false,
            communicationChannels: []
        });

        res.api({});
    });

    // Карточка кампании
    router.get('/api/v1/campaign/item/:campaignId', function (req, res) {
        const {campaignId} = req.params;
        res.api(campaignList.find(item => item.id == campaignId));
    });

    // Обновление данных кампании
    router.post('/api/v1/campaign/item/:campaignId', function (req, res) {
        /*
            STATUSES: [STOPPED, COMPLETED, ACTIVE]
        */

        const {campaignId} = req.params;
        const data = req.body.data;
        console.log('update campaing', campaignId, 'with data', data);

        let campaign = campaignList.find(item => item.id == campaignId);

        if(data.set_status) {
            if(['ACTIVE', 'STOPPED', 'COMPLETED'].indexOf(data.set_status) === -1) {
                console.warn("BAD STATUS, one of 'ACTIVE', 'STOPPED', 'COMPLETED' should be");
            }
            else {
                if(data.set_status === 'ACTIVE') {
                    if(Math.random() > 0.7) {
                        console.warn('Cannot set campaign status to "ACTIVE" because of random error');
                        return res.api('Невозможно запустить кампанию, т.к. произошла рандомная ошибка.');
                    }
                }
                campaign.status = data.set_status;
            }
        }

        if(data.add_communicationChannel || data.remove_communicationChannel) {
            let communicationChannels = new Set(campaign.communicationChannels);
            if(data.add_communicationChannel) communicationChannels.add(data.add_communicationChannel);
            if(data.remove_communicationChannel) communicationChannels.delete(data.remove_communicationChannel);
            campaign.communicationChannels = [...communicationChannels];
        }

        if(data.set_name) campaign.name = data.set_name;
        if(data.set_beginDate) campaign.beginDate = data.set_beginDate;
        if(data.set_endDate) campaign.endDate = data.set_endDate;
        if(data.set_description) campaign.description = data.set_description;
        if(data.set_locked) campaign.locked = data.set_locked;
        if(data.set_groupName) campaign.groupName = data.set_groupName;

        res.api(campaign);
    });

    // Удаляет кампанию
    router.delete('/api/v1/campaign/item/:campaignId', function (req, res) {
        const {campaignId} = req.params;

        let campaignIndex = campaignList.findIndex(item => item.id == campaignId);
        console.log('campaing with id', campaignId, 'deleted', campaignList.splice(campaignIndex, 1));
        res.api({});
    });
};
