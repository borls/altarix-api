'use strict';

import Query from '../../http';
import Http from '../../query';

const NOW_TIME = (new Date()).getTime();
const DAY_MS = 1000 * 60 * 60 * 24;

const subjects = [
    {
        id: 'qwedas',
        avatar: '',
        name: 'Владимир',
        surname: 'Кружков',
        patronomic: 'Сергеевич',
        phone: '89167441430', // or null
        email: null,
        gender: 'man',
        age: 19,
        district: 'cao',
        totalDelivered: 454,
        totalOpened: 12,
        totalTransitions: 2,
        totalSubscribtions: 5,
        totalUnSubscribtions: 2,
        communications: [
            {campaignName: 'Новости Москвы', campaignId: '571a5d03b42b94a16d8b4570', channel: 'Почта', groupName: 'Группа123', groupId: 'dcf80944-bc81-49d8-98c3-9721698a415b', date: new Date(NOW_TIME + (DAY_MS * Math.random() * 80)).getTime()},
            {campaignName: 'Новости Москвы', campaignId: '571a5d03b42b94a16d8b4570', channel: 'Смс', groupName: 'Группа123', groupId: 'dcf80944-bc81-49d8-98c3-9721698a415b', date: new Date(NOW_TIME + (DAY_MS * Math.random() * 80)).getTime()},
            {campaignName: 'Новости Москвы', campaignId: '571a5d03b42b94a16d8b4570', channel: 'Почта', groupName: 'Группа123', groupId: 'dcf80944-bc81-49d8-98c3-9721698a415b', date: new Date(NOW_TIME + (DAY_MS * Math.random() * 80)).getTime()},
            {campaignName: 'Новости Москвы', campaignId: '571a5d03b42b94a16d8b4570', channel: 'Телефон', groupName: 'Группа123', groupId: 'dcf80944-bc81-49d8-98c3-9721698a415b', date: new Date(NOW_TIME + (DAY_MS * Math.random() * 80)).getTime()},
            {campaignName: 'Новости Москвы', campaignId: '571a5d03b42b94a16d8b4570', channel: 'Почта', groupName: 'Группа123', groupId: 'dcf80944-bc81-49d8-98c3-9721698a415b', date: new Date(NOW_TIME + (DAY_MS * Math.random() * 80)).getTime()},
            {campaignName: 'Новости Москвы', campaignId: '571a5d03b42b94a16d8b4570', channel: 'Телефон', groupName: 'Группа123', groupId: 'dcf80944-bc81-49d8-98c3-9721698a415b', date: new Date(NOW_TIME + (DAY_MS * Math.random() * 80)).getTime()},
            {campaignName: 'Новости Москвы', campaignId: '571a5d03b42b94a16d8b4570', channel: 'Смс', groupName: 'Группа123', groupId: 'dcf80944-bc81-49d8-98c3-9721698a415b', date: new Date(NOW_TIME + (DAY_MS * Math.random() * 80)).getTime()},
            {campaignName: 'Новости Москвы', campaignId: '571a5d03b42b94a16d8b4570', channel: 'Почта', groupName: 'Группа123', groupId: 'dcf80944-bc81-49d8-98c3-9721698a415b', date: new Date(NOW_TIME + (DAY_MS * Math.random() * 80)).getTime()},
            {campaignName: 'Новости Москвы', campaignId: '571a5d03b42b94a16d8b4570', channel: 'Почта', groupName: 'Группа123', groupId: 'dcf80944-bc81-49d8-98c3-9721698a415b', date: new Date(NOW_TIME + (DAY_MS * Math.random() * 80)).getTime()},
            {campaignName: 'Новости Москвы', campaignId: '571a5d03b42b94a16d8b4570', channel: 'Телефон', groupName: 'Группа123', groupId: 'dcf80944-bc81-49d8-98c3-9721698a415b', date: new Date(NOW_TIME + (DAY_MS * Math.random() * 80)).getTime()},
            {campaignName: 'Новости Москвы', campaignId: '571a5d03b42b94a16d8b4570', channel: 'Смс', groupName: 'Группа123', groupId: 'dcf80944-bc81-49d8-98c3-9721698a415b', date: new Date(NOW_TIME + (DAY_MS * Math.random() * 80)).getTime()},
            {campaignName: 'Новости Москвы', campaignId: '571a5d03b42b94a16d8b4570', channel: 'Почта', groupName: 'Группа123', groupId: 'dcf80944-bc81-49d8-98c3-9721698a415b', date: new Date(NOW_TIME + (DAY_MS * Math.random() * 80)).getTime()},
            {campaignName: 'Новости Москвы', campaignId: '571a5d03b42b94a16d8b4570', channel: 'Почта', groupName: 'Группа123', groupId: 'dcf80944-bc81-49d8-98c3-9721698a415b', date: new Date(NOW_TIME + (DAY_MS * Math.random() * 80)).getTime()},
            {campaignName: 'Новости Москвы', campaignId: '571a5d03b42b94a16d8b4570', channel: 'Смс', groupName: 'Группа123', groupId: 'dcf80944-bc81-49d8-98c3-9721698a415b', date: new Date(NOW_TIME + (DAY_MS * Math.random() * 80)).getTime()},
            {campaignName: 'Новости Москвы', campaignId: '571a5d03b42b94a16d8b4570', channel: 'Телефон', groupName: 'Группа123', groupId: 'dcf80944-bc81-49d8-98c3-9721698a415b', date: new Date(NOW_TIME + (DAY_MS * Math.random() * 80)).getTime()},
            {campaignName: 'Новости Москвы', campaignId: '571a5d03b42b94a16d8b4570', channel: 'Телефон', groupName: 'Группа123', groupId: 'dcf80944-bc81-49d8-98c3-9721698a415b', date: new Date(NOW_TIME + (DAY_MS * Math.random() * 80)).getTime()},
            {campaignName: 'Новости Москвы', campaignId: '571a5d03b42b94a16d8b4570', channel: 'Почта', groupName: 'Группа123', groupId: 'dcf80944-bc81-49d8-98c3-9721698a415b', date: new Date(NOW_TIME + (DAY_MS * Math.random() * 80)).getTime()},
            {campaignName: 'Новости Москвы', campaignId: '571a5d03b42b94a16d8b4570', channel: 'Смс', groupName: 'Группа123', groupId: 'dcf80944-bc81-49d8-98c3-9721698a415b', date: new Date(NOW_TIME + (DAY_MS * Math.random() * 80)).getTime()},
            {campaignName: 'Новости Москвы', campaignId: '571a5d03b42b94a16d8b4570', channel: 'Телефон', groupName: 'Группа123', groupId: 'dcf80944-bc81-49d8-98c3-9721698a415b', date: new Date(NOW_TIME + (DAY_MS * Math.random() * 80)).getTime()},
            {campaignName: 'Новости Москвы', campaignId: '571a5d03b42b94a16d8b4570', channel: 'Почта', groupName: 'Группа123', groupId: 'dcf80944-bc81-49d8-98c3-9721698a415b', date: new Date(NOW_TIME + (DAY_MS * Math.random() * 80)).getTime()},
            {campaignName: 'Новости Москвы', campaignId: '571a5d03b42b94a16d8b4570', channel: 'Смс', groupName: 'Группа123', groupId: 'dcf80944-bc81-49d8-98c3-9721698a415b', date: new Date(NOW_TIME + (DAY_MS * Math.random() * 80)).getTime()},
            {campaignName: 'Новости Москвы', campaignId: '571a5d03b42b94a16d8b4570', channel: 'Почта', groupName: 'Группа123', groupId: 'dcf80944-bc81-49d8-98c3-9721698a415b', date: new Date(NOW_TIME + (DAY_MS * Math.random() * 80)).getTime()},
            {campaignName: 'Новости Москвы', campaignId: '571a5d03b42b94a16d8b4570', channel: 'Телефон', groupName: 'Группа123', groupId: 'dcf80944-bc81-49d8-98c3-9721698a415b', date: new Date(NOW_TIME + (DAY_MS * Math.random() * 80)).getTime()},
            {campaignName: 'Новости Москвы', campaignId: '571a5d03b42b94a16d8b4570', channel: 'Смс', groupName: 'Группа123', groupId: 'dcf80944-bc81-49d8-98c3-9721698a415b', date: new Date(NOW_TIME + (DAY_MS * Math.random() * 80)).getTime()}
        ]
    }
];

export default function registerGroups_Subjects(router) {
    router.get('/api/v1/group/target-group/item/:groupId/list-filter/:list_filter/subject/list', (req, res) => {
        const { groupId, list_filter } = req.params;
        console.log('request subjects list from', groupId, 'with', list_filter, 'filter');
        res.api(subjects);
    });

    router.get('/api/v1/group/target-group/item/:groupId/list-filter/:list_filter/subject/item/:personId', (req, res) => {
        const { groupId, list_filter, personId } = req.params;
        console.log('request subject', personId, 'from', groupId, 'with', list_filter, 'filter');
        res.api(subjects.find(x => x.id === personId));
    });
};
