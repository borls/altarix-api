export default function registerAuth(router) {
    router.post('/api/v1/user/signin', (req, res) => {
        const token = req.headers.authorization;
        console.log('Token from API', token);
        res.header('Authorization', token);
        res.header('x-pixel-role', 'admin');
        res.api({});
    });

    // router.post('/api/v1/user/check', (req, res) => { // /api/v1/checkLogin
    //     res.header('x-pixel-role', 'admin');
    //     res.api({});
    // });
};
