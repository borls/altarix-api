'use strict';

import Http from '../../query';
import groupExport from '../../../data/group/export';
const groupFilterId = (id) => require(`../../../data/group/filter/${id}`);

export default function registerGroups(router) {
  /**
   * Groups Export
   */

  router.get('/api/v1/group/export/item/:id', function (req, res) {
    console.log(2314, req.body);
    setTimeout(() => res.api(groupExport), 500);
  });
  /**
   * Groups Filter KD
   */
  const rand = (min, max) => Math.floor(min + Math.random() * (max - min));
  //
  // router.get('/api/v1/group/filter', function (req, res) {
  //   const id = rand(0, 10);
  //   setTimeout(() => res.api(groupFilterId(id)), 1000);
  //   console.log('filter variant is', id);
  //   //console.log(JSON.stringify(req.query.params.emailed, null, ' '));
  // });

  /**
   * Groups Import
   */
  // router.post('/api/v1/group/import', function (req, res) {
  //   console.log(req.body);
  //   // здесь проверяется файл на корректность и если все хорошо, то оправляем две строчки
  //   const preview = Object.keys(req.body)[0].split('\n').slice(0, 2);
  //   file = Object.keys(req.body)[0];
  //   res.api([{
  //     "id": "123",
  //     "rows": [preview[0].split(';'), preview[1].split(';'),]
  //   }]);
  // });

  const event = new Http({ table: 'event' });
  router.get('/api/v1/group/event/list', event.getList);
  router.post('/api/v1/group/event/item', event.createItem);
  router.post('/api/v1/group/event/item/:id', event.updateItem);
  router.delete('/api/v1/group/event/item/:id', event.removeItem);

  const attribute = new Http({ table: 'attribute' });
  router.get('/api/v1/group/attribute/list', attribute.getList);
  router.post('/api/v1/group/attribute/item', attribute.createItem);
  router.post('/api/v1/group/attribute/item/:id', attribute.updateItem);
  router.delete('/api/v1/group/attribute/item/:id', attribute.removeItem);
  router.get('/api/v1/group/attribute/format/list', (req, res) => {
    return res.api([
      "date",
      "string",
      "datetime",
      "multiple_string",
      "integer"
    ]);
  });

  const group = new Http({ table: 'group' });
  router.get('/api/v1/group/list', group.getList);
  router.get('/api/v1/group/item/:id', group.getItem);
  router.post('/api/v1/group/item/:id', group.updateItem);
  router.post('/api/v1/group/item', group.createItem);
  router.delete('/api/v1/group/item/:id', group.removeItem);

  let file = '';
  router.post('/api/v1/group/upload', (req, res) => {
    // имитация работы сервера
    file = req.body[0].split('\n');
    res.api([{
      "id": "123",
      "columns": file.length,
      "total": req.body[0].length,
      "rows": file.slice(0,2).map(i => i.split(';'))
    }]);
  });
  router.post('/api/v1/group/import', (req, res) => {
    delete req.body.id;
    req.body.type = 'static';
    req.body.subjects = file
      .slice(+req.body.skipFirst)
      .map(subject => subject.split(';')
        .reduce((res, attribute, i) => ({ ...res, [req.body.fields[i].attribute]: attribute }), {}));
    console.log('--->', req.body);
    return group.createItem(req, res);
  });

  router.get('/api/v1/group/export', (req, res) => {
    setTimeout(() => res.api(groupExport), 500)
  });
  router.get('/api/v1/group/filter', (req, res) => setTimeout(() => res.api(groupFilterId(rand(0, 10))), 1000));


};
