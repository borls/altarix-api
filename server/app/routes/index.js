import registerAuth from './auth';
import registerCampaigns from './campaigns';
import registerCommunicationChannels from './communicationChannels';
import registerGroups from './groups';
import registerStats from './stats';
import registerTemplates from './templates';
import registerUsers from './users';
import registerGroups_Subjects from './groups_subjects';

export {
    registerAuth,
    registerCampaigns,
    registerCommunicationChannels,
    registerGroups,
    registerStats,
    registerTemplates,
    registerUsers,
    registerGroups_Subjects
};
