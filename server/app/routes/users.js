import Http from '../../http';
const path = require('path');

export default function registerUsers(router) {
  const user = new Http({ table: 'user' });
  router.get('/api/v1/user/list', user.getList);
  router.get('/api/v1/user/item/:id', user.getItem);
  router.post('/api/v1/user/item', user.createItem);
  router.post('/api/v1/user/item/:id', user.updateItem);
  router.delete('/api/v1/user/item/:id', user.removeItem);

  const userId = "f4985d73-a074-46f3-b2d6-0395590a3541"; // Густав Юнг
  router.get('/api/v1/profile', (req, res) => user.getItem({ ...req, query: { id: userId } }, res));
  router.post('/api/v1/profile', (req, res) => user.updateItem({ ...req, params: { id: userId } }, res));
  router.get(`/api/v1/profile/avatar/${userId}`, (req, res) => res.sendFile(path.join(__dirname, '..', 'images', 'favicon.png')));
};
