'use strict';

import communication_channels from '../../../data/communication_channels';
import communication_channels_test from '../../../data/communication_channels_test';

export default function registerCommunicationChannels(router) {
    /*
        Каналы коммуникаций, general_analysis
    */

    router.get('/api/v1/communication-channel/list', (req, res) => {
        res.api(communication_channels_test);
        // res.api(['Электронная почта', 'SMS', 'Push сообщение', 'Мобильные приложения', 'Web пространство'].map(randomizeCommunicationChannel));
    });
};
