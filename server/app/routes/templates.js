'use strict';

import Http from '../../http';
const path = require('path');

/*
    Типы блоков, доступные в редакторе
*/
import BLOCK_TYPES from '../../../data/template/block_types';

/*
    Список устройств для предпросмотра
*/
import PREVIEW_DEVICES from '../../../data/template/preview_devices';

/*
    Объект нового шаблона
    Из него создается новый шаблон, путем клонирования
*/
const NEW_TEMPLATE = {
    id: 123, // will be set before creation
    name: 'Новый шаблон',
    image: '/api/v1/template/test_preview',
    canDelete: true,
    canEdit: true,
    style: `
        h1 {
            text-align: center;
            width: 100%;
            color: lightpink;
        }
    `,
    blocks: [
        {
            type: 'Header',
            uid: 'a',
            params: {
                'tag': 'h1',
                'text': 'Hello world!'
            }
        }
    ],
    text: 'Hello world!'
};

/*
    "бд", здесь храняться сохраненные шаблоны
*/
let templates = require('../../../data/template/templates_data.json');

function save_templates() {
    const fs = require('fs');
    fs.writeFile(path.join(__dirname, '../../../data/template/templates_data.json'), JSON.stringify(templates), 'utf8', (err) => {
        if(err) console.log('failed save templates', err);
    });
}

export default function registerTemplates(router) {
    // const template = new Http({table: 'template'});

    // Получить список
    router.get('/api/v1/template/list', (req, res) => {
        res.api(templates);
    });

    // Дублирование шаблона
    router.get('/api/v1/template/item/:id/clone', (req, res) => {
        const id = req.params.id;
        console.log('clone template by id', id);
        const templ = templates.find(x => x.id === id);
        if(!templ) {
            console.log('template not found');
            return res.error(`Template ${id} not found`);
        }

        const newId = (Math.random() * 9999 + '').replace('.', '0');
        const data = Object.assign({}, templ, {id: newId});
        templates.push(data);
        save_templates();
        res.api(data);
    });

    // Получить один элемент
    router.get('/api/v1/template/item/:id', (req, res) => {
        const id = req.params.id;
        console.log('get template by id', id);
        res.api(templates.find(x => x.id === id));
    });

    // Создать элемент
    // 'get' т.к. пользователь не передает никаких данных о новом шаблоне, при этом мы возвращаем новый шаблон
    router.get('/api/v1/template/create', (req, res) => {
        console.log('create template');
        const id = (Math.random() * 9999 + '').replace('.', '0');
        const data = Object.assign({}, NEW_TEMPLATE, {id});
        templates.push(data);
        save_templates();
        res.api(data);
    });

    // Обновляем данные о шаблоне
    router.post('/api/v1/template/item/:id', (req, res) => {
        const id = req.params.id;
        const data = req.body.data;
        console.log('update template', id, data);
        const i = templates.findIndex(x => x.id === id);
        if(i === -1) {
            console.log('template not found');
            res.error('Failed update template');
        }
        templates[i] = Object.assign(templates[i], data, {id});
        save_templates();
        res.api(templates[i]);
    });

    // Удаляем шаблон
    router.delete('/api/v1/template/item/:id', (req, res) => {
        const id = req.params.id;
        console.log('delete template', id);
        const i = templates.findIndex(x => x.id === id);
        if(i === -1) {
            console.log('template not found');
            res.error('Failed delete template');
        }
        templates.splice(i, 1);
        save_templates();
        res.api();
    });

    // Получаем список блоков
    router.get('/api/v1/template/email/block-type/list', (req, res) => {
        res.api(BLOCK_TYPES);
    });

    // Получаем список устройств предпросмотра
    router.get('/api/v1/template/email/device/list', (req, res) => {
        res.api(PREVIEW_DEVICES);
    });

    // Получаем изображение tablet
    router.get('/api/v1/template/email/device/img/ipad', (req, res) => {
        res.sendFile(path.join(__dirname, '../../../data/template/ipad.png'));
    });

    // Получаем изображение browser
    router.get('/api/v1/template/email/device/img/browser', (req, res) => {
        res.sendFile(path.join(__dirname, '../../../data/template/browser.png'));
    });

    // Получаем изображение phone
    router.get('/api/v1/template/email/device/img/iphone', (req, res) => {
        res.sendFile(path.join(__dirname, '../../../data/template/iphone.png'));
    });

    // Получаем изображение, "отрендереное" превью шаблона
    router.get('/api/v1/template/test_preview', (req, res) => {
        res.sendFile(path.join(__dirname, '../../../data/template/email_template_preview.png'));
    });

    // Отправка тестового письма (шаблона) на опред. адрес
    router.get('/api/v1/template/item/:id/test-mail', (req, res) => {
        const id = req.params.id;
        const email = req.query.email;
        console.log(`SEND TEST MAIL FROM ${id} TEMPLATE TO ${email}`);
        res.api();
    });

    router.get('/api/v1/template/images/:file', (req, res) => {
        res.sendFile(path.join(__dirname, '../../../data/template/img_blocks/'+req.params.file));
    });
};
