export function randomizeCommunicationChannel(name) {
    const MAX_TOTAL = 2000000,
          MIN_TOTAL = 1000;
    const MIN_ACTIVE = 100;

    const total = MIN_TOTAL + Math.floor(Math.random() * (MAX_TOTAL - MIN_TOTAL));
    const active = MIN_ACTIVE +  Math.floor(Math.random() * (total - MIN_ACTIVE));

    const man = Math.floor(Math.random() * total) +1;
    const woman = total - man;

    let AGES = ['0-6', '7-14', '15-18', '19-30', '31-49', '50+'];
    AGES.sort(() => { return Math.random() > 0.5 ? -1 : 0; });

    let ages = [];

    let totalLeft = total;
    let activeLeft = active;
    AGES.some(type => {
        if(totalLeft <= 0) return true;
        let ageTotal = Math.floor(Math.random() * totalLeft) +1;
        let ageActive = Math.min(ageTotal, Math.floor(Math.random() * activeLeft));

        totalLeft -= ageTotal;
        activeLeft -= ageActive;

        ages.push({
            type,
            total: ageTotal,
            active: ageActive
        });
    });

    let DISTRICTS = ['cao', 'vao', 'svao', 'tao', 'zao', 'nao', 'uzao', 'uao', 'uvao', 'sao', 'szao', 'zelao'];
    DISTRICTS.sort(() => { return Math.random() > 0.5 ? -1 : 0; });

    let districts = [];

    totalLeft = total;
    activeLeft = active;
    DISTRICTS.some(name => {
        if(totalLeft <= 0) return true;
        let districtTotal = Math.floor(Math.random() * totalLeft) +1;
        let districtActive = Math.min(districtTotal, Math.floor(Math.random() * activeLeft));

        totalLeft -= districtTotal;
        activeLeft -= districtActive;

        districts.push({
            name,
            total: districtTotal,
            active: districtActive
        });
    });

    return {
        name,
        total,
        active,
        man,
        woman,
        ages,
        districts
    };
}
