import r from 'rethinkdb';
import config from 'config';
import xss from 'xss';


export default class Service {
  constructor(props) {
    this.table = props.table;

    this.liveUpdates = this.liveUpdates.bind(this);

    this.getList = this.getList.bind(this);
    this.getItem = this.getItem.bind(this);
    this.createItem = this.createItem.bind(this);
    this.updateItem = this.updateItem.bind(this);
    this.removeItem = this.removeItem.bind(this);
  }

  connect() {
    return r.connect(config.get('rethinkdb'));
  }

  // На клиенте кто-то должен ловить событие 'event-change'
  // И вызывать диспатч с соответствующим изменением
  liveUpdates(io) {
    console.log('Setting up listener...');
    this.connect()
      .then(conn => {
        r
          .table(this.table)
          .changes().run(conn, (err, cursor) => {
            console.log('Listening for changes...');
            cursor.each((err, change) => {
              console.log('Change detected', change);
              io.emit(`${this.table}-change`, change);
            });
          });
      });
  }

  getList() {
    return this.connect()
      .then(conn => r
        .table(this.table)
        .orderBy(r.desc('created')).run(conn)
        .then(cursor => cursor.toArray()));

  }

  createItem(item) {
    return this.connect()
      .then(conn => {
        item.created = new Date().getTime() / 1000 << 0;
        item.text = xss(item.text);
        return r
          .table(this.table)
          .insert(item).run(conn)
          .then(response => Object.assign({}, item, { id: response.generated_keys[ 0 ] }));
      });
  }

  updateItem(id, item) {
    item.updated = new Date();
    item.text = xss(item.text);
    return this.connect()
      .then(conn => r
        .table(this.table)
        .get(id).update(item).run(conn)
        .then(() => item));
  }

  removeItem(id) {
    return this.connect()
      .then(conn => r
        .table(this.table)
        .get(id).delete().run(conn)
        .then(() => ({ id, deleted: true })));
  }
  getItem(id) {
    return this.connect()
      .then(conn => r
        .table(this.table)
        .get(id).run(conn));
  }
}