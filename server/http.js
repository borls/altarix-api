import Service from './service';

class Query {
  constructor(props) {
    this.service = new Service(props);

    this.getList = this.getList.bind(this);
    this.getItem = this.getItem.bind(this);
    this.createItem = this.createItem.bind(this);
    this.updateItem = this.updateItem.bind(this);
    this.removeItem = this.removeItem.bind(this);
  }

  getList(req, res) {
    this.service.getList()
      .then((list) => res.api(list))
      .catch((error) => {
        res.status(400);
        res.api(req.body, 400, error);
      });
  }
  getItem(req, res) {
    this.service.getItem(req.query.id)
      .then((item) => res.api(item))
      .catch((error) => {
        res.status(400);
        res.api(req.body, 400, error);
      });
  }

  createItem(req, res) {
    this.service.createItem(req.body)
      .then((item) => res.api(item))
      .catch((error) => {
        res.status(400);
        res.api(req.body, 400, error);
      });
  }

  updateItem(req, res) {
    const id = req.params.id; //req.query.id
    this.service.updateItem(id, req.body)
      .then((item) => res.api(item))
      .catch(error => {
        res.status(400);
        res.api(req.body, 400, error);
      });
  }

  removeItem(req, res) {
    const id = req.params.id; //req.query.id
    this.service.removeItem(id)
      .then((item) => res.api(item))
      .catch(error => {
        res.status(400);
        res.api(req.body, 400, error);
      });
  }
}

export default Query;
