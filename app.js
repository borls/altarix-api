import app from './server/app';
import config from 'config';

const e = config.get('express');

console.log('config:', e);

// e.host = '127.0.0.1';

app.listen(e.port, /*e.host*/ '127.0.0.1', () => {});
